package com.cryptobionic.lmlr;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.cryptobionic.lmlr.movie.MovieContent;
import com.cryptobionic.lmlr.movie.MovieContent.*;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class Connect extends Activity {
	
    JSch jsch = new JSch();
    Button connectButton, playButton;
    public static Session session;
    public static Channel channel;
	int connection_status;
	public static String hostname, username, password;
	
	

	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connect_screen);  
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        
        connectButton = (Button) findViewById(R.id.connect_button);
		this.connectButton.setOnClickListener(new OnClickListener() {
			public void onClick(final View v) {
				System.out.println("At least button was called!");
				new VerySlowTask().execute();
			}
		});
    }
    
	
	
	private class VerySlowTask extends AsyncTask<String, Long, Void> {

		private ProgressDialog dialog= new ProgressDialog(Connect.this);

		protected void onPreExecute() {
			this.dialog.setMessage("Connecting...");
			this.dialog.show();
		}
		
		protected Void doInBackground(final String... args) {
							
	        	EditText etMsg1 = (EditText) findViewById(R.id.hostname_text);
	    		hostname = etMsg1.getText().toString();
	    		EditText etMsg2 = (EditText) findViewById(R.id.username_text);
	    		username = etMsg2.getText().toString();
	    		EditText etMsg3 = (EditText) findViewById(R.id.password_text);
	    		password = etMsg3.getText().toString();	 
	    	
				// For testing	
	    		hostname="";
	    		username="";
	    		password="";
	    		
	    		try {
	        		session = jsch.getSession(username, hostname, 22);
	        		session.setPassword(password);
	        		        
	        		// Avoid asking for key confirmation
	        		Properties prop = new Properties();
	        		prop.put("StrictHostKeyChecking", "no");
	        		session.setConfig(prop);

	        		session.connect();

	        		System.out.println("STATUS: " + session.isConnected());
	        		switch_screen();
	    		} catch (Exception e) { System.err.println("Error connecting: " + e);}	    		
				return null;
		}
	
		@Override
		protected void onProgressUpdate(Long... value) 
		{
		}
		protected void onPostExecute(final Void unused) {
			if (dialog != null) {
				dialog.dismiss();
			    dialog = null;
			    
			}
			System.out.println("DONE");
		}
	}
    public void switch_screen()
	{
		final Context context = this;
		Intent intent = new Intent(context, MediaType.class);
		startActivity(intent);
	}
    
    public static String getLines()
    {
		String command = "/home/lmlr/vlc-bin/getLines.sh";
		
		try {	
			ChannelExec channelssh = (ChannelExec) 
            session.openChannel("exec");      				
			channelssh.setCommand(command);
			channelssh.connect();
			
			InputStream in = channelssh.getInputStream();
			byte[] tmp = new byte[10000];
			while(true)
			{
				while(in.available()>0)
				{
					int i = in.read(tmp, 0, 10000);
					if(i < 0) break;
					return( new String(tmp, 0, i));
				}   
			 }

    	} catch (Exception e2) { 
    		// TODO Auto-generated catch block
    		System.out.println("EXCEPTION: " + e2);
    	}
		return "Could not fetch data";
    }

    public static String[] returnMovies(String movieText)
    {
		String[] movieName = movieText.split("::");	
		for (int i = 0; i < movieName.length; i++) {
		System.out.println("MOVIENAME: " + movieName[i]); }
		return movieName;
    }

    /*
	public static int parseLines()
	{
		String lines = getLines();
		System.out.println("lines:" + lines + ":");
		String line[] = lines.split("\n");
		System.out.println("getlines:" + line[0] + "end");
		// change this when I figure out how to grab the number of movies correctly
		return 3;//Integer.parseInt(line[0]);
	}*/

	public static void addMovie()
	{
		System.out.println("getMovies: " + getMovies());
		String movieText = getMovies();
		System.out.println("movieText: " + movieText);
		
		String[] movieArray = returnMovies(movieText);
		for (int i = 0; i < movieArray.length; i++)
		{
			System.out.println("movieArray[" + i + "]: " + movieArray[i]);
		}

		int movie_num = movieArray.length;
				
		for (int i = 0; i <= movie_num; i++)
		{
			String j = Integer.toString(i+1);
			System.out.println("i: " + i + " j: " + j + " Movie: " + movieArray[i]);
			MovieContent.addItem(new MovieItem(j, movieArray[i]));
		}
	}
	
    public static String getMovies()
    {		
		try {	
			String command = "/home/lmlr/vlc-bin/list.sh";
			ChannelExec channelssh = (ChannelExec) 
            session.openChannel("exec");      				
			channelssh.setCommand(command);
			channelssh.connect();
			
			InputStream in = channelssh.getInputStream();
			byte[] tmp = new byte[10000];
			while(true)
			{
				while(in.available()>0)
				{
					int i = in.read(tmp, 0, 10000);
					if(i < 0) break;
					return( new String(tmp, 0, i));
				}   
			 }
			
    	} catch (Exception e2) { 
    		// TODO Auto-generated catch block
    		System.out.println("EXCEPTION: " + e2);
    	}	
		return "Could not fetch data";
    }
    
    public static void playMovie(String movieName)
	{
    	String command = "/home/lmlr/vlc-bin/play.sh " + movieName;
		
		try {	
			ChannelExec channelssh = (ChannelExec) 
            session.openChannel("exec");      				
			channelssh.setCommand(command);
			channelssh.connect();
			
    	} catch (Exception e2) { 
    		// TODO Auto-generated catch block
    		System.out.println("EXCEPTION: " + e2);
    	}
	}
    
    public static void pauseMovie(String movieName)
	{
    	try{
			String command = "DISPLAY=:0 xdotool key space;"; 
			channel = session.openChannel("exec");
			((ChannelExec)channel).setCommand(command); 
			channel.setInputStream(null);
			((ChannelExec)channel).setErrStream(System.err);
			channel.connect();
		} catch (Exception e) {
			System.err.println("Error: " + e);
		}
	}
    
    public static void powerOff()
	{
		try{
			String command = "DISPLAY=:0 xset dpms force off;"; 
			channel = session.openChannel("exec");
			((ChannelExec)channel).setCommand(command); 
			channel.setInputStream(null);
			((ChannelExec)channel).setErrStream(System.err);
			channel.connect();
		} catch (Exception e) {
			System.err.println("Error: " + e);
		}
	}
	
    public static void powerOn()
	{
		try{
			String command = "DISPLAY=:0 xset dpms force on; DISPLAY=:0 xdotool key a;"; 
			channel = session.openChannel("exec");
			((ChannelExec)channel).setCommand(command); 
			channel.setInputStream(null);
			((ChannelExec)channel).setErrStream(System.err);
			channel.connect();
		} catch (Exception e) {
			System.err.println("Error: " + e);
		}
	}
    
    
    static String readInputStream() {
		try{
		InputStream in = channel.getInputStream();
			byte[] tmp = new byte[10000];
			while(true)
			{
				while(in.available()>0)
				{
					int i = in.read(tmp, 0, 10000);
					if(i < 0) break;
					return( new String(tmp, 0, i));
				}   
			 }
			
		} catch (Exception e) {
			return("Error: " + e);
		}	
	}
    
    
    
    @Override
    protected void onResume() {

    	super.onResume();
    	// register Listener for SensorManager and Accelerometer sensor
    	//mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    	}  
     
    /**
     * Maybe this could be used to grab pictures if the thing dom did doesn't work
     * 
     * @throws MalformedURLException
     */
    
    public void downloadGraph() throws MalformedURLException
    {    	
    	URL url = new URL("http://54.220.232.79/GraphStats.png");

    	//create the new connection

    	HttpURLConnection urlConnection;
    	try {
			urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setRequestMethod("GET");
		    urlConnection.setDoOutput(true);
		    urlConnection.connect();
		    File SDCardRoot = new File("/storage/sdcard0/Download/");
		    File file = new File(SDCardRoot,"GraphStats.png");
		    FileOutputStream fileOutput = new FileOutputStream(file);
		    InputStream inputStream = urlConnection.getInputStream();
		    int totalSize = urlConnection.getContentLength();
		    int downloadedSize = 0;
		    byte[] buffer = new byte[1024];
		    int bufferLength = 0; //used to store a temporary size of the buffer
		    while ( (bufferLength = inputStream.read(buffer)) > 0 ) 
		    {
		    	fileOutput.write(buffer, 0, bufferLength);
		    	downloadedSize += bufferLength;
		    	int progress=(int)(downloadedSize*100/totalSize);
		    }
		    fileOutput.close();
		//    displayGraph();  
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    }
    
        
}
