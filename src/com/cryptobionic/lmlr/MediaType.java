package com.cryptobionic.lmlr;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MediaType extends Activity {

	Button movie_button;
	Button tv_button;
	Button music_button;
	
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.media_choice);  
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
                     
        movie_button = (Button) findViewById(R.id.movie_button);
		this.movie_button.setOnClickListener(new OnClickListener() {
			public void onClick(final View v) {
				System.out.println("Movie button was called!");
				// Go to the MediaFileList thing with movie in mind
				switch_screen_movie();
				
			}
		});
		
		tv_button = (Button) findViewById(R.id.tv_button);
		this.tv_button.setOnClickListener(new OnClickListener() {
			public void onClick(final View v) {
				System.out.println("TV button was called!");
				//switch_screen_movie();
				switch_screen_movie();

				// Go to the MediaFileList thing with movie in mind
			}
		});
		
		music_button = (Button) findViewById(R.id.music_button);
		this.music_button.setOnClickListener(new OnClickListener() {
			public void onClick(final View v) {
				System.out.println("Music button was called!");
				// Go to the MediaFileList thing with movie in mind
				switch_screen_movie();
			}
		});
    }
	
    public void switch_screen_movie()
	{
    	
    	System.out.println("In switch screen");
    	final Context context = this;
		Intent intent2 = new Intent(context, MovieListActivity.class);
		intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent2);
	}
}
