package com.cryptobionic.lmlr;

import java.io.InputStream;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.cryptobionic.lmlr.movie.MovieContent;
import com.jcraft.jsch.ChannelExec;

/**
 * A fragment representing a single Movie detail screen. This fragment is either
 * contained in a {@link MovieListActivity} in two-pane mode (on tablets) or a
 * {@link MovieDetailActivity} on handsets.
 */
public class MovieDetailFragment extends Fragment {
	Button playButton, pauseButton;
	String movieName = "999";
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";

	/**
	 * The dummy content this fragment is presenting.
	 */
	private MovieContent.MovieItem mItem;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public MovieDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments().containsKey(ARG_ITEM_ID)) {
			// Load the dummy content specified by the fragment
			// arguments. In a real-world scenario, use a Loader
			// to load content from a content provider.
			mItem = MovieContent.ITEM_MAP.get(getArguments().getString(
					ARG_ITEM_ID));
		}
	}
	
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_movie_detail,
				container, false);

		// So i think I should change this.  It defaults to text view, but I don't want just text
		// Show the dummy content as text in a TextView.
		/*if (mItem != null) {
			((TextView) rootView.findViewById(R.id.movie_detail))
					.setText(mItem.content);
		}*/
		
		((TextView) rootView.findViewById(R.id.movie_detail_text)).setText(mItem.content);
		
		movieName = ((TextView)rootView.findViewById(R.id.movie_detail_text)).getText().toString();

		playButton = (Button) rootView.findViewById(R.id.play_button);
		pauseButton = (Button) rootView.findViewById(R.id.pause_button);

		pauseButton.setEnabled(false);

		
		this.playButton.setOnClickListener(new OnClickListener() {
			public void onClick(final View v) {
				System.out.println("Play button was called!");
				System.out.println("Movie to be played....hmmmm: " + movieName);
				Connect.playMovie(movieName);
				playButton.setEnabled(false);
				pauseButton.setEnabled(true);
			}
		});
		
		this.pauseButton.setOnClickListener(new OnClickListener() {
			public void onClick(final View v) {
				System.out.println("Play button was called!");
				System.out.println("Movie to be played....hmmmm: " + movieName);
				Connect.playMovie(movieName);
				pauseButton.setEnabled(false);
				playButton.setEnabled(true);
			}
		});
		
		
		
		if (mItem != null)
		{
			//rootView.findViewById(R.id.movie_detail).setTag(mItem.content);
		}
		
		return rootView;
	}
	
	
}
