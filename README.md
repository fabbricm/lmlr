# README #

LMLR - Linux Media Library Remote

* LMLR is an android application that allows you to connect to a media server running Linux.  
* It's aim is to serve as a remote for a server on your home network connected to a television.


### Setup ###

To use this application, your server must have the back end installed, which can be found in this project.

### Help ###

cameronfabbri@gmail.com